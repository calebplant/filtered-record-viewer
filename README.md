# Filtered Record Viewer

## Overview

A generic LWC that we can use to view and filter object records. Presents up to 2000 records paginated with a page size of 20.

## Demo (~80 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=VNPJwepSo9w)

## Usage

The user provides the name of the object they wish to view, an array of fields to display, and an array of the fields they want to be able to filter on. See screenshot below for an example.

### Sample Inputs

![Component Inputs](media/inputs.png)

### Unfiltered List

![Unfiltered List](media/unfiltered-list.png)

### Filtered List

![Filtered List](media/filtered-list.png)