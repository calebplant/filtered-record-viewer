import { api, LightningElement } from 'lwc';

const BOOLEAN_FIELDS = ['checkbox', 'toggle'];
const NUMERIC_FIELDS = ['date', 'datetime', 'time', 'number'];
const TEXT_FIELDS = ['email', 'tel', 'text', 'url'];
const RANGE_FIELDS = ['between'];

export default class FilterRow extends LightningElement {
    
    @api criteriaOptions;
    @api typeDict;
    @api index;
    @api hasMultipleRows;

    @api
    getRowData(){
        // console.log('accountCriteriaRow :: getRowData');
        let data = {};
        data['fieldName'] = this.selectedCriteria;
        data['fieldType'] = this.criteriaType;
        data['searchType'] = this.selectedSearchType;
        data['minValue'] = this.minValue;
        data['maxValue'] = this.maxValue;
        data['value'] = this.value;
        data['index'] = this.index;
        // console.log(data);
        return data;
    }

    @api
    setRowData(data){
        // console.log('accountCriteriaRow :: setRowData');
        // console.log(JSON.parse(JSON.stringify(data)));
        this.selectedCriteria = data['fieldName'];
        this.criteriaType = data['fieldType'];
        this.selectedSearchType = data['searchType'];
        this.minValue = data['minValue'];
        this.maxValue = data['maxValue'];
        this.value = data['value'];
    }

    @api
    resetData(){
        // console.log('accountCriteriaRow :: resetData');
        this.selectedCriteria = undefined;
        this.criteriaType = 'text';
        this.selectedSearchType = undefined;
        this.minValue = undefined;
        this.maxValue = undefined;
        this.value = undefined;
    }
    
    selectedCriteria;
    criteriaType = 'text';
    selectedSearchType;
    minValue;
    maxValue;
    value;

    get isBoolean() {
        return BOOLEAN_FIELDS.includes(this.criteriaType);
    }

    get isNumeric() {
        return NUMERIC_FIELDS.includes(this.criteriaType);
    }

    get isText() {
        return TEXT_FIELDS.includes(this.criteriaType);
    }

    get booleanOptions() {
        return [
            {label: 'Equals', value:'equal'}
        ];
    }

    get numericOptions() {
        return [
            {label: 'Equals', value:'equal'},
            {label: 'Greater Than', value:'greater'},
            {label: 'Less Than', value:'less'},
            {label: 'Between', value:'between'},
        ];
    }

    get textOptions() {
        return [
            {label: 'Equals', value:'equal'},
            {label: 'Contains', value:'contain'},
        ]
    }

    get isRange() {
        return RANGE_FIELDS.includes(this.selectedSearchType);
    }

    get selectTypeDisabled() {
        // Disables search type until a field name is selected
        return !this.selectedCriteria;
    }

    get valueInputDisabled() {
        // Disabled value input until a search type is selected
        return !this.selectedSearchType;
    }

    get removeDisabled() {
        // Prevent user from deleting row if only one row exists
        return (this.index == 0 && !this.hasMultipleRows);
    }

    // Handlers
    handleCriteriaChange(event) {
        console.log('accountCriteriaRow :: handleCriteriaChange');
        this.resetRowValues();
        this.selectedCriteria = event.detail.value;
        this.criteriaType = this.typeDict[event.detail.value];
        // console.log('criteriaType: ' + this.criteriaType);
        if(BOOLEAN_FIELDS.includes(this.criteriaType)) {
            // If it's a boolean field, we can bypass search type since it's just either true or false
            this.selectedSearchType = 'boolean';
        }
        console.log(this.selectedCriteria);
    }

    handleSearchTypeChange(event) {
        console.log('accountCriteriaRow :: handleSearchTypeChange');
        let criteria = this.selectedCriteria;
        this.resetRowValues();
        this.selectedCriteria = criteria;
        this.selectedSearchType = event.detail.value;
        // console.log(this.selectedSearchType);
    }

    handleMinValueChange(event) {
        this.minValue = event.detail.value;
    }

    handleMaxValueChange(event) {
        this.maxValue = event.detail.value;
    }

    handleValueChange(event) {
        if(BOOLEAN_FIELDS.includes(this.criteriaType)) {
            // console.log(event.target.checked);
            this.value = event.target.checked;
        } else {
            // console.log(event.detail.value);
            this.value = event.detail.value;
        }
    }

    handleRemove(event) {
        console.log('FilterRow :: handleRemove');
        // console.log(this.index);
        this.dispatchEvent(new CustomEvent('removerow', {detail: this.index}));
    }

    // Utils
    resetRowValues() {
        this.selectedCriteria = this.selectedSearchType = this.value = this.minValue = this.maxValue = undefined;
    }
}