import { api, LightningElement, wire } from 'lwc';
import getRecords from '@salesforce/apex/FilteredRecordViewerController.getObjectRecords';

export default class FilteredRecordViewer extends LightningElement {

    @api objectName = 'Account';
    @api displayedRecordFields = [
        {label: 'Name', fieldName: 'Name'},
        {label: 'State', fieldName: 'ShippingState'},
        {label: 'Country', fieldName: 'ShippingCountry'},
        {label: 'Record Type', fieldName: 'RecordType.Name'},
        {label: 'Industry', fieldName: 'Industry'},
    ];
    @api filterFieldOptions = [
        'Id', 'Name', 'ShippingState', 'ShippingCountry', 'RecordType.Name',
        'Industry', 'LastViewedDate', 'AnnualRevenue'
    ];

    @wire(getRecords, {objectName: '$objectName', fields: '$queryFields'})
    fetchRecords(response) {
        if(response.data) {
            // console.log('Records from server:');
            // console.log(response.data);
            let flattenedRecords = response.data.map(r => this.flattenJSON(r));
            // console.log('Flattened data:');
            // console.log(JSON.parse(JSON.stringify(flattenedRecords)));
            this.records = flattenedRecords;
        }
        if(response.error) {
            console.log('Error fetching records:');
            console.log(response.error);
        }
    }

    records;
    queryFields;

    get cardTitle() {
        return this.objectName ? `${this.objectName} List` : 'Object List';
    }

    get displayedFields() {
        return this.displayedRecordFields;
    }

    connectedCallback() {
        console.log('FilteredRecordViewer :: connectedCallback');
        // console.log(new Set([...this.displayedRecordFields.map(f => f.fieldName), ...this.filterFieldOptions]));

        // Combine displayed fields and filtered fields to determine which fields we put into our record query
        this.queryFields = Array.from(new Set([...this.displayedRecordFields.map(f => f.fieldName), ...this.filterFieldOptions]));
    }

    // Utils

    // Flatten JSON data for use in datatable. Ex:
    // {Name: 'Acc0', RecordType: {Id: '123', Name: 'RT1'}} => {Name: 'Acc0', RecordType.Id: '123', RecordType.Name: 'RT1'}
    flattenJSON(data) {
        var result = {};
        function recurse (cur, prop) {
            if (Object(cur) !== cur) {
                result[prop] = cur;
            } else if (Array.isArray(cur)) {
                for(var i=0, l=cur.length; i<l; i++)
                    recurse(cur[i], prop + "[" + i + "]");
                if (l == 0)
                    result[prop] = [];
            } else {
                var isEmpty = true;
                for (var p in cur) {
                    isEmpty = false;
                    recurse(cur[p], prop ? prop+"."+p : p);
                }
                if (isEmpty && prop)
                    result[prop] = {};
            }
        }
        recurse(data, "");
        return result;
    }
}