import { api, wire, LightningElement } from 'lwc';
import { subscribe, MessageContext } from 'lightning/messageService';
import UPDATE_FILTERS_MC from "@salesforce/messageChannel/UpdateFilterChannel__c";


export default class ListOfRecord extends LightningElement {
    @api
    get records() {
        return this._records;
    }

    set records(value) {
        this.filteredRecords = value;
        this._records = value;
        this.updatePagination();
    }

    @api recordFields;
    @api resultPerPage = 20;

    currentPage;
    totalPages;
    paginationSet = false;
    filteredRecords;

    // private
    _records;

    subscription = null;
    @wire(MessageContext)
    messageContext;

    connectedCallback() {
        this.handleSubscribe();
    }

    renderedCallback() {
        console.log('ListOfRecord :: renderedCallback');
    }

    get tableColumns() {
        // TODO? maintain order
        let columns = [];
        this.recordFields.forEach(eachField => {
            columns.push({
                label: eachField.label,
                fieldName: eachField.fieldName,
                type: 'text'
            });
        })
        console.log('Columns:');
        console.log(JSON.parse(JSON.stringify(columns)));
        return columns;
    }

    get displayedRecords() {
        console.log('displayedRecords');
        if(this.filteredRecords) {
            let start = Number(this.currentPage) * Number(this.resultPerPage);
            let end = Number(start) + Number(this.resultPerPage);
            return this.filteredRecords.slice(start, end);
        }
    }

    get nextDisabled() {
        return (this.currentPage == (this.totalPages - 1) || this.currentPage == undefined);
    }

    get previousDisabled() {
        return (this.currentPage === 0 || this.currentPage == undefined);
    }

    get pageLabel() {
        if(this.filteredRecords && this.resultPerPage) {
            let start = Number(this.currentPage) * Number(this.resultPerPage) + 1;
            let end = Number(start) + Number(this.resultPerPage) - 1;
            end = end > this.filteredRecords.length ? this.filteredRecords.length : end;

            return 'Displaying ' + start + ' - ' + end + ' of ' + this.filteredRecords.length;
        }
        return '';
    }

    // Handlers
    handleSort(event) {
        console.log('ListOfRecord :: handleSort');
    }

    handlePageChange(event) {
        console.log('ListOfRecord :: handlePageChange');
        switch(event.currentTarget.name){
            case 'previous':
                this.currentPage--;
                break;
            case 'next':
                this.currentPage++;
                break;
        }
    }

    // Utils
    updatePagination() {
        console.log('ListOfRecord :: updatePagination');
        if(this.filteredRecords) {
            this.currentPage = 0;
            this.totalPages = Math.ceil(this.filteredRecords.length / this.resultPerPage);
        }
    }
    
    handleSubscribe() {
        if (this.subscription) {
            return;
        }
        this.subscription = subscribe(this.messageContext, UPDATE_FILTERS_MC, (message) => {
            console.log('ListOfRecord :: handle message');
            this.updateFilters(message.filters);
        });
    }

    updateFilters(filters) {
        console.log('ListOfRecord :: updateFilters');
        console.log(filters);

        let query = {};

        filters.forEach(eachFilter => {
            let fieldName = eachFilter.fieldName;
            switch(eachFilter.searchType) {
                case 'equal':
                    query[fieldName] = {
                        equal: eachFilter.value
                    };
                    break;
                case 'contain':
                    query[fieldName] = {
                        contain: eachFilter.value
                    };
                    break;
                case 'greater':
                    query[fieldName] = {
                        greater: eachFilter.value
                    };
                    break;
                case 'less':
                    query[fieldName] = {
                        less: eachFilter.value
                    };
                    break;
                case 'between':
                    query[fieldName] = {
                        min: eachFilter.minValue,
                        max: eachFilter.maxValue
                    };
                    break;
                case 'boolean':
                    query[fieldName] = {
                        boolean: eachFilter.value
                    };
                    break;
            }
        });

        console.log('query');
        console.log(query);

        try {
            let filteredData = this._records.filter( (item) => {
                for (let key in query) {
                    if (item[key] === undefined) {
                        return false;
                    }
                    // Done
                    else if (query[key]['min'] && query[key]['max']) {
                        if (item[key] < query[key]['min'] || item[key] > query[key]['max']) {
                            return false;
                        }
                    }
                    // Done
                    else if (query[key]['equal']) {
                        if(item[key] !== query[key]['equal']) {
                            return false;
                        }
                    }
                    // Done
                    else if(query[key]['contain']) {
                        console.log('contains');
                        if(!item[key].includes(query[key]['contain'])) {
                            return false;
                        }
                    }
                    // Done
                    else if (query[key]['greater']) {
                        if (parseFloat(item[key]) < parseFloat(query[key]['greater'])) {
                            return false;
                        }
                    }
                    // Done
                    else if (query[key]['less']) {
                        console.log('less:');
                        console.log(item[key]);
                        console.log(query[key]['less']);
                        if (parseFloat(item[key]) > parseFloat(query[key]['less'])) {
                            return false;
                        }
                    }
                    // Boolean
                    else if (query[key]['boolean']) {
                        if (item[key] != query[key]['boolean']) {
                            return false;
                        }
                    }
                }
                return true;
            });
    
            console.log('filtered Data:');
            console.log(filteredData);

            this.filteredRecords = filteredData;
            this.updatePagination();
        } catch(err) {
            console.log(err);
        }
    }
}