import { api, LightningElement, track, wire } from 'lwc';
import { MessageContext, publish } from 'lightning/messageService';
import UPDATE_FILTERS_MC from "@salesforce/messageChannel/UpdateFilterChannel__c";
import getObjectFields from '@salesforce/apex/FilteredRecordViewerController.getVisibleObjectFields'

export default class FilterObject extends LightningElement {
    @api objectName;
    @api filterFieldOptions;

    criteriaOptions;
    typeDict;
    @track numOfCriteria = ['0'];
    criteriaRows;
    // numOfCriteriaValue = 0;

    @wire(MessageContext)
    messageContext;

    @wire(getObjectFields, {objectName: '$objectName', targetFilters: '$filterFieldOptions'})
    getObjectFields(response) {
        if(response.data) {
            this.criteriaOptions = [];
            this.typeDict = {};
            console.log('Successfully retreived fields.');
            console.log(response.data);

            this.criteriaOptions = response.data.map(eachField => {
                return {
                    label: eachField.label,
                    value: eachField.value
                };
            });
            // Map field type for each input field (ex: 'text', 'number')
            response.data.forEach(eachField => {
                this.typeDict[eachField.value] = eachField.fieldType
            });

            // console.log(this.criteriaOptions);
            // console.log(this.typeDict);
        }
        if(response.error) {
            console.log('Error retreiving fields!');
            console.log(response.error);
        }
    }

    disconnectedCallback() {
        releaseMessageContext(this.context);
    }

    // get numOfCriteriaOptions() {
    //     return [
    //         {label: 1, value: 1},
    //         {label: 2, value: 2},
    //         {label: 3, value: 3},
    //         {label: 4, value: 4},
    //         {label: 5, value: 5}
    //     ];
    // }

    get hasMultipleRows() {
        return this.numOfCriteria.length > 1;
    }

    // Handlers

    // Grab each filter and pass them to listOfRecord to filter the displayed records
    handleApplyFilters() {
        console.log('FilterObject :: handleApplyFilters');
        let criteriaRows = this.template.querySelectorAll('c-filter-row'); // get each row
        let filters = [...criteriaRows].map(eachRow => eachRow.getRowData()); // pull filter data from each row
        // console.log('filters: ');
        // console.log(JSON.parse(JSON.stringify(filters)));
        let message = {filters: filters};
        publish(this.messageContext, UPDATE_FILTERS_MC, message); // send filters to listOfRecord
    }

    // Grab each filter and clear it
    handleResetFilters() {
        console.log('FilterObject :: handleResetFilters');
        let message = {filters: []};
        publish(this.messageContext, UPDATE_FILTERS_MC, message); // tell listOfRecord to remove filters

        this.numOfCriteria = ['0'];
        // this.numOfCriteriaValue = 1;
        let criteriaRows = this.template.querySelectorAll('c-filter-row');
        criteriaRows.forEach(eachRow => eachRow.resetData()); // reset each row
    }

    // handleNumOfCriteriaSelected(event) {
    //     console.log('FilterObject :: handleNumOfCriteriaSelected');
    //     this.numOfCriteria = [];
    //     for(let i=0; i < event.detail.value; i++) {
    //         this.numOfCriteria.push('');
    //     }
    //     // this.numOfCriteriaValue = this.numOfCriteria.length;
    // }

    handleAddRow(event) {
        console.log('FilterObject :: handleAddRow');
        // console.log(JSON.parse(JSON.stringify(this.numOfCriteria)));

        // this.numOfCriteriaValue++;
        // this.numOfCriteria.push(String(this.numOfCriteriaValue));
        this.numOfCriteria.push(String(this.numOfCriteria.length));
        // console.log(JSON.parse(JSON.stringify(this.numOfCriteria)));
    }

    handleRemoveRow(event) {
        console.log('FilterObject :: handleRemoveRow');
        // console.log('event.detail: ' + event.detail);
        let criteriaRows = this.template.querySelectorAll('c-filter-row');
        // Store each filter's data, excluding the removed row
        let rowData = [...criteriaRows].map(eachRow => eachRow.getRowData())
                    .filter(eachRow => eachRow.index != event.detail);
        // console.log('rowData');
        // console.log(JSON.parse(JSON.stringify(rowData)));

        // Walk through each row and set the data
        for(let i=0; i < criteriaRows.length; i++) {
            if(i < rowData.length) {
                criteriaRows[i].setRowData(rowData[i]);
            }
            // else {
                // criteriaRows[i].resetData();
            // }
        }

        // Remove last row display because we removed a filter
        let newLength = this.numOfCriteria.length - 1;
        this.numOfCriteria = ['0'];
        for(let i=1; i < newLength; i++) {
            this.numOfCriteria.push(String(i));
        }

        // let newLength = this.numOfCriteria.length - 1;
        // this.numOfCriteriaValue--;
        // this.numOfCriteria = ['0'];
        // for(let i=1; i <= this.numOfCriteriaValue; i++) {
        //     this.numOfCriteria.push(String(i));
        // }
    }
}