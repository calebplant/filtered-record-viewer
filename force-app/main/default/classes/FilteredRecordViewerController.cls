public with sharing class FilteredRecordViewerController {
    
    /*
        Returns up to 2000 object records for the object passed in objectName

        Parameters:
        @   objectName - object name to query for (ex: Account, Contact)
        @   fields - String list of fields to query for (ex: ['Name', 'Industry', 'RecordType.Name'])
    */
    @AuraEnabled(cacheable=true)
    public static List<SObject> getObjectRecords(String objectName, List<String> fields) {
        System.debug('START getObjectRecords');
        
        if(objectName != null && fields != null) {
            String query = 'SELECT ' + String.join(fields, ',') + ' FROM ' + String.escapeSingleQuotes(objectName) + ' LIMIT 2000';
            System.debug(query);
            return Database.query(query);
        }
        return null;
    }

    /*
        Returns a list of FieldOption for each field we want to be able to filter against.

        Parameters:
        @   objectName - object name whose fields we will examine (ex: Account, Contact)
        @   targetFilters - String list of field names we want to filter against (ex: ['Name', 'Industry', 'ShippingCountry'])
    */
    @AuraEnabled(cacheable=true)
    public static List<FieldOption> getVisibleObjectFields(String objectName, List<String> targetFilters)
    {
        List<FieldOption> result = new List<FieldOption>();

        if(objectName != null && targetFilters != null) {
            System.debug(targetFilters);
            // walk through the object's fields
            SObjectType sObjType = ((SObject) Type.forName(objectName).newInstance()).getSObjectType();
            for (SObjectField eachField : sObjType.getDescribe().fields.getMap().values()) {
                Schema.DescribeFieldResult F = eachField.getDescribe();
                // Add a FieldOption if the field is included in our targetFilters
                if(F.isAccessible() && F.isFilterable() && targetFilters.contains(F.getName())) {
                    result.add(new FieldOption(F.getLabel(), F.getName(), F.getType()));
                }
            }
            // Include record type if it was passed in targetFilters
            if(includeRecordType(targetFilters)) {
                result.add(new FieldOption('Record Type', 'RecordType.Name', 'text'));
            }
            result.sort();
        }

        return result;
    }

    private static Boolean includeRecordType(List<String> targetFilters) {
        return targetFilters.contains('RecordType.Name');
    }
}
