public with sharing class FieldOption implements Comparable{
    @AuraEnabled
    public String label; // ex: 'Shipping Country', 'Name'
    @AuraEnabled
    public String value; // ex: 'ShippingCountry', 'Name'
    @AuraEnabled
    public String fieldType; // ex: 'text', 'toggle', 'datetime', ...

    // @AuraEnabled
    // public Integer typeOrdinal;

    public FieldOption(String fieldLabel, String fieldName, Schema.DisplayType type)
    {
        label = fieldLabel;
        value = fieldName;
        fieldType = setTypeValue(type);
        // typeOrdinal = type.ordinal();
    }

    public FieldOption(String fieldLabel, String fieldName, String type)
    {
        label = fieldLabel;
        value = fieldName;
        fieldType = type;
    }

    private static String setTypeValue(Schema.DisplayType fieldType)
    {
        // Check the field type to determine return value.
        // See: https://developer.salesforce.com/docs/atlas.en-us.apexcode.meta/apexcode/apex_enum_Schema_DisplayType.htm
        
        // System.debug(fieldType);
        switch on fieldType {
            when ID {
                return 'text';
            }
            when address {
                return 'text';
            }
            when Boolean {
                // return 'checkbox';
                return 'toggle';
            }
            when Currency {
                return 'number';
            }
            when Date {
                return 'date';
            }
            when Datetime {
                return 'datetime';
            }
            when Email {
                return 'email';
            }
            when Integer, Double {
                return 'number';
            }
            when Percent {
                return 'number';
            }
            when Phone {
                return 'tel';
            }
            when Picklist {
                return 'text';
            }
            when MultiPicklist {
                return 'text';
            }
            when Reference {
                return 'text';
            }
            when String {
                return 'text';
            }
            when TextArea {
                return 'text';
            }
            when Time {
                return 'time';
            }
            when URL {
                return 'url';
            } 
        }
        return null;
    }

    // Allows us to sort on label
    public Integer CompareTo(Object ObjToCompare) {
        FieldOption that = (FieldOption)ObjToCompare;
        if (this.label > that.label) return 1;
        if (this.label < that.label) return -1;
        return 0;
    }
}
